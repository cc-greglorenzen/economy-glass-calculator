<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://www.managedword.com
 * @since      1.0.0
 *
 * @package    Economy_Glass_Calculator
 * @subpackage Economy_Glass_Calculator/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
