<?php

/**
 * Fired during plugin activation
 *
 * @link       https://www.managedword.com
 * @since      1.0.0
 *
 * @package    Economy_Glass_Calculator
 * @subpackage Economy_Glass_Calculator/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Economy_Glass_Calculator
 * @subpackage Economy_Glass_Calculator/includes
 * @author     Christopher Frazier <chris.frazier@managedword.com>
 */
class Economy_Glass_Calculator_Activator
{

    /**
     * Short Description. (use period)
     *
     * Long Description.
     *
     * @since    1.0.0
     */
    public static function activate()
    {
        self::update_products();
    }

    public static function update_products()
    {
        $gravity_defaults = array(
            'id' => '7',
            'display_title' => false,
            'display_description' => false,
            'disable_woocommerce_price' => 'no',
            'price_before' => '',
            'price_after' => '',
            'disable_calculations' => 'no',
            'disable_label_subtotal' => 'yes',
            'disable_label_options' => 'yes',
            'disable_label_total' => 'yes',
            'disable_anchor' => 'no',
            'label_subtotal' => 'Subtotal',
            'label_options' => 'Options',
            'label_total' => 'Total',
            'use_ajax' => 'no',
        );

        $measurement_defaults = array(
		    'calculator_type' => 'area-dimension',
		    'dimension' =>
		    array(
		        'pricing' =>
		        array(
		            'enabled' => 'no',
		            'label' => '',
		            'unit' => 'in',
		            'calculator' =>
		            array(
		                'enabled' => 'no',
		            ),
		            'inventory' =>
		            array(
		                'enabled' => 'no',
		            ),
		            'weight' =>
		            array(
		                'enabled' => 'no',
		            ),
		        ),
		        'length' =>
		        array(
		            'enabled' => 'yes',
		            'label' => 'Required Length',
		            'unit' => 'in',
		            'editable' => 'yes',
		            'options' =>
		            array(
		                0 => '',
		            ),
		        ),
		        'width' =>
		        array(
		            'enabled' => 'no',
		            'label' => 'Required Width',
		            'unit' => 'in',
		            'editable' => 'yes',
		            'options' =>
		            array(
		                0 => '',
		            ),
		        ),
		        'height' =>
		        array(
		            'enabled' => 'no',
		            'label' => 'Required Height',
		            'unit' => 'in',
		            'editable' => 'yes',
		            'options' =>
		            array(
		                0 => '',
		            ),
		        ),
		    ),
		    'area' =>
		    array(
		        'pricing' =>
		        array(
		            'enabled' => 'no',
		            'label' => '',
		            'unit' => 'sq cm',
		            'calculator' =>
		            array(
		                'enabled' => 'no',
		            ),
		            'inventory' =>
		            array(
		                'enabled' => 'no',
		            ),
		            'weight' =>
		            array(
		                'enabled' => 'no',
		            ),
		        ),
		        'area' =>
		        array(
		            'label' => 'Required Area',
		            'unit' => 'sq cm',
		            'editable' => 'yes',
		            'options' =>
		            array(
		                0 => '',
		            ),
		        ),
		    ),
		    'area-dimension' =>
		    array(
		        'pricing' =>
		        array(
		            'enabled' => 'yes',
		            'label' => '',
		            'unit' => 'sq. ft.',
		            'calculator' =>
		            array(
		                'enabled' => 'yes',
		            ),
		            'inventory' =>
		            array(
		                'enabled' => 'no',
		            ),
		            'weight' =>
		            array(
		                'enabled' => 'no',
		            ),
		        ),
		        'length' =>
		        array(
		            'label' => 'Length',
		            'unit' => 'in',
		            'editable' => 'yes',
		            'options' =>
		            array(
		                0 => '',
		            ),
		        ),
		        'width' =>
		        array(
		            'label' => 'Width',
		            'unit' => 'in',
		            'editable' => 'yes',
		            'options' =>
		            array(
		                0 => '',
		            ),
		        ),
		    ),
		    'area-linear' =>
		    array(
		        'pricing' =>
		        array(
		            'enabled' => 'no',
		            'label' => '',
		            'unit' => 'in',
		            'calculator' =>
		            array(
		                'enabled' => 'no',
		            ),
		            'inventory' =>
		            array(
		                'enabled' => 'no',
		            ),
		            'weight' =>
		            array(
		                'enabled' => 'no',
		            ),
		        ),
		        'length' =>
		        array(
		            'label' => 'Length',
		            'unit' => 'in',
		            'editable' => 'yes',
		            'options' =>
		            array(
		                0 => '',
		            ),
		        ),
		        'width' =>
		        array(
		            'label' => 'Width',
		            'unit' => 'in',
		            'editable' => 'yes',
		            'options' =>
		            array(
		                0 => '',
		            ),
		        ),
		    ),
		    'area-surface' =>
		    array(
		        'pricing' =>
		        array(
		            'enabled' => 'no',
		            'label' => '',
		            'unit' => 'ha',
		            'calculator' =>
		            array(
		                'enabled' => 'no',
		            ),
		            'inventory' =>
		            array(
		                'enabled' => 'no',
		            ),
		            'weight' =>
		            array(
		                'enabled' => 'no',
		            ),
		        ),
		        'length' =>
		        array(
		            'label' => '',
		            'unit' => 'km',
		            'editable' => 'yes',
		            'options' =>
		            array(
		                0 => '',
		            ),
		        ),
		        'width' =>
		        array(
		            'label' => '',
		            'unit' => 'km',
		            'editable' => 'yes',
		            'options' =>
		            array(
		                0 => '',
		            ),
		        ),
		        'height' =>
		        array(
		            'label' => '',
		            'unit' => 'km',
		            'editable' => 'yes',
		            'options' =>
		            array(
		                0 => '',
		            ),
		        ),
		    ),
		    'volume' =>
		    array(
		        'pricing' =>
		        array(
		            'enabled' => 'no',
		            'label' => '',
		            'unit' => 'ml',
		            'calculator' =>
		            array(
		                'enabled' => 'no',
		            ),
		            'inventory' =>
		            array(
		                'enabled' => 'no',
		            ),
		            'weight' =>
		            array(
		                'enabled' => 'no',
		            ),
		        ),
		        'volume' =>
		        array(
		            'label' => 'Required Volume',
		            'unit' => 'ml',
		            'editable' => 'yes',
		            'options' =>
		            array(
		                0 => '',
		            ),
		        ),
		    ),
		    'volume-dimension' =>
		    array(
		        'pricing' =>
		        array(
		            'enabled' => 'no',
		            'label' => '',
		            'unit' => 'ml',
		            'calculator' =>
		            array(
		                'enabled' => 'no',
		            ),
		            'inventory' =>
		            array(
		                'enabled' => 'no',
		            ),
		            'weight' =>
		            array(
		                'enabled' => 'no',
		            ),
		        ),
		        'length' =>
		        array(
		            'label' => 'Length',
		            'unit' => 'in',
		            'editable' => 'yes',
		            'options' =>
		            array(
		                0 => '',
		            ),
		        ),
		        'width' =>
		        array(
		            'label' => 'Width',
		            'unit' => 'in',
		            'editable' => 'yes',
		            'options' =>
		            array(
		                0 => '',
		            ),
		        ),
		        'height' =>
		        array(
		            'label' => 'Height',
		            'unit' => 'in',
		            'editable' => 'yes',
		            'options' =>
		            array(
		                0 => '',
		            ),
		        ),
		    ),
		    'volume-area' =>
		    array(
		        'pricing' =>
		        array(
		            'enabled' => 'no',
		            'label' => '',
		            'unit' => 'ml',
		            'calculator' =>
		            array(
		                'enabled' => 'no',
		            ),
		            'inventory' =>
		            array(
		                'enabled' => 'no',
		            ),
		            'weight' =>
		            array(
		                'enabled' => 'no',
		            ),
		        ),
		        'area' =>
		        array(
		            'label' => 'Area',
		            'unit' => 'sq cm',
		            'editable' => 'yes',
		            'options' =>
		            array(
		                0 => '',
		            ),
		        ),
		        'height' =>
		        array(
		            'label' => 'Height',
		            'unit' => 'in',
		            'editable' => 'yes',
		            'options' =>
		            array(
		                0 => '',
		            ),
		        ),
		    ),
		    'weight' =>
		    array(
		        'pricing' =>
		        array(
		            'enabled' => 'no',
		            'label' => '',
		            'unit' => 'lbs',
		            'calculator' =>
		            array(
		                'enabled' => 'no',
		            ),
		            'inventory' =>
		            array(
		                'enabled' => 'no',
		            ),
		            'weight' =>
		            array(
		                'enabled' => 'no',
		            ),
		        ),
		        'weight' =>
		        array(
		            'label' => 'Required Weight',
		            'unit' => 'lbs',
		            'editable' => 'yes',
		            'options' =>
		            array(
		                0 => '',
		            ),
		        ),
		    ),
		    'wall-dimension' =>
		    array(
		        'pricing' =>
		        array(
		            'enabled' => 'no',
		            'label' => '',
		            'unit' => 'sq cm',
		            'calculator' =>
		            array(
		                'enabled' => 'no',
		            ),
		            'inventory' =>
		            array(
		                'enabled' => 'no',
		            ),
		            'weight' =>
		            array(
		                'enabled' => 'no',
		            ),
		        ),
		        'length' =>
		        array(
		            'label' => 'Distance around your room',
		            'unit' => 'in',
		            'editable' => 'yes',
		            'options' =>
		            array(
		                0 => '',
		            ),
		        ),
		        'width' =>
		        array(
		            'label' => 'Height',
		            'unit' => 'in',
		            'editable' => 'yes',
		            'options' =>
		            array(
		                0 => '',
		            ),
		        ),
		    ),
		);

        $products = get_posts(array(
            'post_type' => 'product',
            'posts_per_page' => -1
        ));

        // The Loop
        foreach ($products as $product) {
            update_post_meta($product->ID, '_gravity_form_data', $gravity_defaults);
			update_post_meta($product->ID, '_wc_price_calculator', $measurement_defaults);
        }
    }
}
