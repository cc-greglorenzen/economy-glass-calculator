<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://www.managedword.com
 * @since      1.0.0
 *
 * @package    Economy_Glass_Calculator
 * @subpackage Economy_Glass_Calculator/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Economy_Glass_Calculator
 * @subpackage Economy_Glass_Calculator/includes
 * @author     Christopher Frazier <chris.frazier@managedword.com>
 */
class Economy_Glass_Calculator_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
