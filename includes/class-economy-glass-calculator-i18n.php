<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://www.managedword.com
 * @since      1.0.0
 *
 * @package    Economy_Glass_Calculator
 * @subpackage Economy_Glass_Calculator/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Economy_Glass_Calculator
 * @subpackage Economy_Glass_Calculator/includes
 * @author     Christopher Frazier <chris.frazier@managedword.com>
 */
class Economy_Glass_Calculator_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'economy-glass-calculator',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
